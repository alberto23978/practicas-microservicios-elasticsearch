package com.microservice.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservice.demo.model.User;

@RestController
@RequestMapping("/user")
public class UserController {
	
	List<User> users = new ArrayList<User>();
	
	@GetMapping
	public List<User> get() {
		return users;
	}
	
	@PostMapping
	public String save(@RequestBody User newUser) {
		User u = new User(newUser.getName(),newUser.getEmail());
		users.add(u);
		return ("¡El usuario se inserto de forma exitosa!");
	}

}
