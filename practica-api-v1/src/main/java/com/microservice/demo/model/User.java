package com.microservice.demo.model;

public class User {

	private static int CONN = 1;
	
	private int id;
	private String name;
	private String email;
	
	public User() {
	}
	
	public User(String name, String email) {
		this.id = CONN++;
		this.name = name;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
