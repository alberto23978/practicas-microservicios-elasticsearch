package com.microservice.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticaApiV1Application {

	public static void main(String[] args) {
		SpringApplication.run(PracticaApiV1Application.class, args);
	}

}
